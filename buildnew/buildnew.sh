#!/bin/bash

# 2013-2015 (c) Etersoft www.etersoft.ru
# Author: Vitaly Lipatov <lav@etersoft.ru>
# Public domain

#. /usr/share/eterbuild/korinf/common
#load_mod

# HACK
# local korinf
#KORINFPATH=$(pwd)/../korinf

#cd $(dirname $0)

#rm -rf /tmp/.private/lav/RPM/SOURCES/*


WBYACL=$(pwd)/watch.by.acl
GEAR=git.alt
WORKDIR=$TMPDIR/korinf-builder
GITUSER=$USER

if [ ! -r $WBYACL ] ; then
	wget http://watch.altlinux.org/pub/watch/by-acl/$GITUSER.txt -O $WBYACL
fi

mkdir -p $WORKDIR

docmd()
{
	echo "$ $@"
	$@
}

rebuild()
{
	# FIXME: предполагаем, что репозиторий называется, как и пакет
	docmd rpmgp $GEAR -g $package && cd $package || return
	docmd gpull gear sisyphus || return
	docmd rpmrb $GEAR $newversion || return
}

build_next()
{
	prev=
	build=
	while read package version newversion src ; do
		echo -n "$package -> $newversion... "
		grep -q "^$package	" $WBYACL.exclude-broken 2>/dev/null && { echo "excluded-broken, skipped" ; continue ; }
		grep -q "^$package	" $WBYACL.exclude 2>/dev/null && { echo "excluded, skipped" ; continue ; }
		grep -q "^$package	$version	$newversion" $WBYACL.done 2>/dev/null && { echo "already built, skipped" ; continue ; }
		# skip
		[ "$prev" = "$package" ] && continue
		echo
		build=1
		break
	done < <(cat $WBYACL | grep -v "#" | cut -f1-3)

	prev="$package"

	# no build
	[ -n "$build" ] || return

	echo "cd $WORKDIR"
	cd $WORKDIR || exit
	rm -rf $WORKDIR/$package
	if rebuild ; then
		rm -rfv $WORKDIR/$package
		echo "$package	$version	$newversion" >>$WBYACL.done
	else
		echo "Skip broken $package"
		echo "$package	$version	$newversion" >>$WBYACL.exclude-broken
	fi
}

echo >$WBYACL.exclude-broken

while build_next ; do
	true
done
