#!/bin/bash -x

# 2013-2015, 2017 (c) Etersoft www.etersoft.ru
# Author: Vitaly Lipatov <lav@etersoft.ru>
# Public domain

# FIXME: see shx, fix ln
. /usr/share/eterbuild/korinf/common

load_mod spec rpm git spec


WBYACL=$(pwd)/list.txt
CHANGELOG="- $(cat message.txt)"
. ./task.txt
WORKDIR=$TMPDIR/korinf-builder
GITUSER=$USER

mkdir -p $WORKDIR

docmd()
{
	echo "$ $@"
	$@
}

rebuild()
{
	# FIXME: предполагаем, что репозиторий называется, как и пакет
	docmd rpmgp $GEAR -g $package && cd $package || return
	if [ -n "$BRANCH" ] ; then
		git checkout -b $BRANCH remotes/gear/$BRANCH || fatal
	fi
	SPECNAME=$(get_gear_spec)
	[ -s "$SPECNAME" ] || fatal "Can't find spec $SPECNAME"
	# TODO: nice increment release
	inc_subrelease $SPECNAME
	QUIET=1 add_changelog_helper "$CHANGELOG" $SPECNAME || fatal "add_changelog failed, do not commit anything"
	git add $SPECNAME || fatal
	gammit --no-edit || fatal
	#docmd gpull gear sisyphus || return
	#docmd rpmrb $GEAR $newversion || return
}

list_pkg()
{
	cat $WBYACL | grep -v "#" | cut -f1 -d" "
}

while read package; do
	[ -n "$package" ] || continue
	echo "cd $WORKDIR"
	cd $WORKDIR || exit
	rm -rf $WORKDIR/$package
	rebuild </dev/null || "fatal with $package"
	cd - >/dev/null
done < <(list_pkg)

LISTS=
for i in $(list_pkg) ; do
	LISTS="$LISTS $WORKDIR/$i"
done

CMD=rpmbs
if [ "$1" = "rpmbsh" ] ; then
	shift
	CMD=rpmbsh
	#KORINFSKIPRELEASECHECK=1 $CMD $GEAR -b $BRANCH $LISTS -u
	NOSTRICT_UNPACKAGED=1 KORINFSKIPRELEASECHECK=1 $CMD $GEAR $LISTS -i || fatal
	CMD=rpmbs
fi
NOSTRICT_UNPACKAGED=1 KORINFSKIPRELEASECHECK=1 $CMD $GEAR $LISTS -a $TASK "$@"
