#!/bin/bash

# 2013-2015 (c) Etersoft www.etersoft.ru
# Author: Vitaly Lipatov <lav@etersoft.ru>
# Public domain

#. /usr/share/eterbuild/korinf/common
#load_mod

# HACK
# local korinf
#KORINFPATH=$(pwd)/../korinf

#cd $(dirname $0)

#rm -rf /tmp/.private/lav/RPM/SOURCES/*


WBYACL=$(pwd)/watch.by.acl
INPROGRESS=$(pwd)/inprogress
GEAR=git.alt
WORKDIR=$TMPDIR/korinf-builder
GITUSER="($USER|baraka|shpigor|elly|pv|canis)"

if [ ! -r $WBYACL ] ; then
	curl http://git.altlinux.org/beehive/stats/Sisyphus-i586/ftbfs-joined | grep -P "\s$GITUSER" > $WBYACL
fi

mkdir -p $WORKDIR

docmd()
{
	echo "$ $@"
	$@
}

rebuild()
{
	# FIXME: предполагаем, что репозиторий называется, как и пакет
	docmd rpmgp $GEAR -g $package && cd $package || return
	echo "$package" >$INPROGRESS
	docmd gpull gear sisyphus || return
	docmd rpmbsh -i || return
}

buildprogress()
{
	cd $package || return
	docmd rpmbsh git.alt -i -u || return
}

build_next()
{
	build=
	while read package version num other ; do
		echo -n "$package-%version $num... "
		grep -q "^$package" $WBYACL.exclude 2>/dev/null && { echo "excluded, skipped" ; continue ; }
		grep -q "^$package	$version" $WBYACL.done 2>/dev/null && { echo "already built, skipped" ; continue ; }
		echo
		build=1
		break
	done < <(cat $WBYACL | grep -v "#")

	# no build
	[ -n "$build" ] || return

	echo "cd $WORKDIR"
	cd $WORKDIR || exit
	rm -rf $WORKDIR/$package/
	rebuild || return

	rm -rfv $WORKDIR/$package/
	echo "$package	$version" >>$WBYACL.done
}

# check if we in build progress
package=$(cat $INPROGRESS 2>/dev/null)
if [ -n "$package" ] ; then
	echo "cd $WORKDIR"
	cd $WORKDIR || exit
	buildprogress
	exit
fi

while build_next ; do
	true
done
