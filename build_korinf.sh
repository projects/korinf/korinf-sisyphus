#!/bin/sh

# Создаёт каталог проекта и укладывает туда файлы по ссылкам

# FIXME: get_distro_list переписать, чтобы . была по порядку, а не в конце списка

. /usr/share/eterbuild/korinf/common
#load_mod

TOPDIR=$(realpath $(dirname $0) )
cd $TOPDIR

# HACK
# local korinf
KORINFPATH=$(pwd)/../korinf

SISYPHUSTO=/var/ftp/pub/Korinf
GEARDIR=/tmp/korinf-sisyphus-gears
WORKBRANCH=sisyphus
DISTR="all"

if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
    echo
    echo "Usage: $0 [-f pkgname] [-d DISTR] [korinf args]"
    echo " -f  force build pkgname"
    echo " -d  build for target DISTR only"
    exit
fi

if [ "$1" = "-f" ] && [ -n "$2" ] ; then
    FORCEONLY="$2"
    shift 2
fi

if [ "$1" = "-F" ] && [ -n "$2" ] ; then
    FORCEONLY="$2"
    korinfopt="-F"
    shift 2
fi

if [ "$1" = "-d" ] && [ -n "$2" ] ; then
    DISTR="$2"
    shift 2
fi

KORINFARG="$1"

mkdir -p $SISYPHUSTO
test -d $SISYPHUSTO || fatal "No $SISYPHUSTO dir"

mkdir -p "$GEARDIR"
test -d "$GEARDIR" || fatal "No $GEARDIR dir"


# FIXME: general function for get list without comments, empty lines and so on
get_list()
{
	cat "$@" | grep -v "^#" | grep -v "^\$"
}

copy_project_files()
{
local projfile="$1"
shift
set_rebuildlist
CMDRE=$(get_distro_list $REBUILDLIST)
[ -z "$CMDRE" ] && fatal "build list is empty"

for dist in $CMDRE ; do
    distfrom=$FROMSISYPHUS/$dist
    test -d $distfrom || continue
    copy_files_by_mask $distfrom $TARGETKORINF/$projfile/$dist $@
done
}

jump_to_repo()
{
	local repourl=$1
	local repo=$2
	cd $GEARDIR/$repo 2>/dev/null && return 1

	docmd cd $GEARDIR/ || return 1
	docmd git clone $repourl $repo || { [ -n "$FORCEONLY" ] && fatal "can't clone $repourl" || return 1 ; }
	docmd cd $repo
	docmd git checkout $WORKBRANCH || docmd git checkout -b $WORKBRANCH origin/$WORKBRANCH || return 1 #fatal "can't checkout branch"
	return 0
}

do_pull()
{
	# check repo
	git rev-parse HEAD >/dev/null || { warning "check repo failed" ; return 1; }

	# Lav: a kind of hack?
	# save TAG for last commit
	CURTAG=$(git rev-parse HEAD)

	# try pull (with rebase) and exit if all up-to-date
	docmd gpull -c origin $WORKBRANCH && return 1

	# gpull can ends fatally (broken connect and so on with false status, so check tag also
	NEWTAG=$(git rev-parse HEAD)
	[ "$CURTAG" = "$NEWTAG" ] && { warning "$(pwd): last commit is not changed after update" ; return 1; }

	# were changes
	return 0
}


# return 0 (true) if there is update
check_gear_repo_and_cd()
{
	local PKGNAME=$1
	# http://git.altlinux.org/gears/N/NAME.git
	local REPOPATH="$(initial_letter $PKGNAME)/$PKGNAME.git"
	RREPO="gears/$REPOPATH"
	jump_to_repo $GIRARURL/$RREPO $PKGNAME && return # need work
	do_pull && return # need work
}

publish_and_build()
{
        local pkgname=$1
        local param=$2
        # TODO: move to korinf command
        export ETERDESTSRPM=$SISYPHUSTO/sources
        #export GNUPGHOME=/var/empty
        # FIXME: what with GPG sign? (see %_gpg_path in ~/.rpmmacros)
        # FIXME: rpm --addsign prompt for Enter ever if there is empty password
        docmd rpmbs
        #docmd rpmpub $SISYPHUSTO/sources
        # FIXME korinf: with sources/ without sources
        local korinfarg=$KORINFARG
        [ "$param" = "install" ] && korinfarg="-b"
        docmd $KORINFPATH/bin/korinf $pkgname $DISTR $SISYPHUSTO $korinfarg $korinfopt
}

check_gears_file()
{
mkdir -p $GEARDIR/
for gearsfile in gears/* ; do
    test -f $gearsfile || continue
    gearname=$(basename $gearsfile)
    echo
    echo "Make $gearname repo...."
    GIRARURL=http://$gearname

# TODO: add support for install param
    get_list $gearsfile | while read i param; do
        [ -n "$param" ] && echo -e "Package $i with command '$param'." || echo -e "Package $i."
        if [ -n "$FORCEONLY" ] ; then
            [ "$FORCEONLY" = "all" ] || [ "$FORCEONLY" = "$i" ] || continue
            check_gear_repo_and_cd $i
            publish_and_build $i $param
        else
            check_gear_repo_and_cd $i && publish_and_build $i $param
        fi
        sleep 2
    done
    #rm -rf $TARGETKORINF/$projfile-tmp
    #copy_project_files $projfile-tmp $LISTPKG || fatal
    #mv $TARGETKORINF/$projfile-tmp $TARGETKORINF/$projfile
done
}

# already run guard
PIDFILE=$GEARDIR/build_korinf.pid
TASKPID=$(cat $PIDFILE 2>/dev/null)
if [ -n "$TASKPID" ] ; then
	# exit if already runned
	[ -r "/proc/$TASKPID" ] && { echo "Already in progress with pid $TASKPID..." ; exit ; }
	# Remove stalled pid
	rm -f $PIDFILE || fatal
fi

echo $$ >$PIDFILE
check_gears_file || fatal "There was errors in check_gears_file"
rm -f $PIDFILE || fatal

$(dirname $0)/make_projects_repo.sh
