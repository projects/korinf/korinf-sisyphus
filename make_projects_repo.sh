#!/bin/sh

# Создаёт каталог проекта и укладывает туда файлы по ссылкам
# В качестве параметра можно указать конкретный проект

. /usr/share/eterbuild/korinf/common
#load_mod

cd $(dirname $0)

FROMSISYPHUS=/var/ftp/pub/Korinf
TARGETKORINF=/var/ftp/pub/Korinf

mkdir -p $TARGETKORINF
test -d $TARGETKORINF || fatal

# FIXME: general function for get list without comments, empty lines and so on
get_list()
{
	cat "$@" | grep -v "^#" | grep -v "^\$"
}

# FIXME: move to right place
# http://stackoverflow.com/questions/2564634/bash-convert-absolute-path-into-relative-path-given-a-current-directory
get_relative_path()
{
	python -c "import os.path; print os.path.relpath('$1', '$2')"
}

# Make links to needed files
copy_files_by_mask()
{
local from="$1"
local to="$2"
shift 2

for i in $@ ; do
    f=$(echo $from/$i*)
    if [ "$f" = "$from/$i*" ] ; then
        # Deb package hack (always low case)
        local lowreg=$(echo $i | tr [A-Z] [a-z])
        f=$(echo $from/$lowreg*)
        [ "$f" = "$from/$lowreg*" ] && continue
    fi
    mkdir -p $to
    echo -e "\t$from"
    for ff in $f ; do
        ln -s -t $to $(get_relative_path $ff $to)
        echo -e "\t\t$(basename $ff)"
    done
done
}


copy_project_files()
{
local projfile="$1"
shift
set_rebuildlist
#REBUILDLIST=Ubuntu/11.04
CMDRE=$(get_distro_list $REBUILDLIST)
[ -z "$CMDRE" ] && fatal "build list is empty"

for dist in $CMDRE ; do
    distfrom=$FROMSISYPHUS/$dist
    test -d $distfrom || continue
    copy_files_by_mask $distfrom $TARGETKORINF/$projfile/$dist $@

    # source packages
    distfrom=$FROMSISYPHUS/sources/$dist
    test -d $distfrom || continue
    copy_files_by_mask $distfrom $TARGETKORINF/$projfile/sources/$dist $@
done
}

my_remove(){
    while [ -e "$1" ] ; do
        rm -rf "$1"
    done
}

PROJ="projects/*"
if [ -n "$1" ] ; then
    PROJ=projects/$1
    test -s $PROJ || fatal "Project $PROJ is unexists"
fi

for projfile in $PROJ ; do
    LISTPKG=`echo $(get_list $projfile)`
    echo -e "Make $projfile project with packages:\n\t$LISTPKG"
    my_remove $TARGETKORINF/${projfile}-tmp
    copy_project_files $projfile-tmp $LISTPKG || fatal
    ##Hack for gluster rm bug
    my_remove $TARGETKORINF/$projfile
    mv $TARGETKORINF/$projfile-tmp $TARGETKORINF/$projfile
done
